﻿using Microcharts;
using StockMarketApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entry = Microcharts.Entry;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SkiaSharp;
using StockMarketApp.Models;
using System.Globalization;
using XFGloss;


namespace StockMarketApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StockChartTab : ContentPage
	{
        private StockViewModel StockVm;

        public StockChartTab()
		{
			InitializeComponent();
            StockVm = StockViewModel.instance;
            BindingContext = StockVm;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            SetChart(30);
            SetChart(100);
        }

        private void SetChart(int days)
        {
            List<Entry> entries = new List<Entry>();
            var i = days;
            var label = "";

            if (!Equals(StockVm.stocks, null) && StockVm.stocks.Count > 0)
            {
                foreach (Stock item in StockVm.stocks)
                {
                    if (i > 0)
                    {
                        if (days % 3 == 0)
                        {
                            label = i % 3 == 0 ? item.high : "";
                        }
                        else
                        {
                            label = i % 5 == 0 ? item.high : "";
                        }
                        entries.Add(new Entry(float.Parse(item.high, CultureInfo.InvariantCulture.NumberFormat))
                        {
                            ValueLabel = label,
                            Color = SKColor.Parse("#266489")
                        });
                    }
                    i--;
                }

                if (days == 30)
                {
                    chart30View.Chart = new LineChart
                    {
                        Entries = entries
                    };
                }
                else if (days == 100)
                {
                    chart100View.Chart = new LineChart
                    {
                        Entries = entries
                    };
                }
            }
        }

    }
}