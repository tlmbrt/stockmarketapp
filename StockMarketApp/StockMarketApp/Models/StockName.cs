﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace StockMarketApp.Models
{
    public class StockNameResponse
    {
        [JsonProperty("bestMatches")]
        public List<StockName> data { get; set; }
    }

    public class StockName
    {
        [JsonProperty("1. symbol")]
        public string symbol { get; set; }

        [JsonProperty("2. name")]
        public string name { get; set; }

        [JsonProperty("3. type")]
        public string type { get; set; }

        [JsonProperty("4. region")]
        public string region { get; set; }

        [JsonProperty("5. marketOpen")]
        public string hoursOpen { get; set; }

        [JsonProperty("6. marketClose")]
        public string hoursClose { get; set; }

        [JsonProperty("7. timezone")]
        public string timeZone { get; set; }

        [JsonProperty("8. currency")]
        public string currency { get; set; }

        [JsonProperty("9. matchScore")]
        public string matchScore { get; set; }
    }
}
