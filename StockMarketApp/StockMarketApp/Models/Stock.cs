﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StockMarketApp.Models
{
    public class StockResponse
    {
        [JsonProperty("Time Series (Daily)")]
        public Dictionary<string, Stock> data { get; set; }

        [JsonProperty("Meta Data")]
        public MetaInfo info { get; set; }
    }

    public class Stock
    {
        [JsonProperty("1. open")]
        public string open { get; set; }

        [JsonProperty("2. high")]
        public string high { get; set; }

        [JsonProperty("3. low")]
        public string low { get; set; }

        [JsonProperty("4. close")]
        public string close { get; set; }

        [JsonProperty("5. volume")]
        public string volume { get; set; }

        public string day { get; set; }
    }

    public class MetaInfo
    {
        [JsonProperty("1. Information")]
        public string info { get; set; }

        [JsonProperty("2. Symbol")]
        public string symbol { get; set; }

        [JsonProperty("3. Last Refreshed")]
        public string updatedAt { get; set; }

        [JsonProperty("4. Output Size")]
        public string size { get; set; }

        [JsonProperty("5. Time Zone")]
        public string timeZone { get; set; }
    }

    public class StockProperty
    {
        public string name { get; set; }

        public string high { get; set; }

        public string low { get; set; }
    }
}
