﻿using Newtonsoft.Json;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using StockMarketApp.Models;
using StockMarketApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFGloss;

namespace StockMarketApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StockListTab : ContentPage
	{
        public string search;

        private StockViewModel StockVm;

        public StockListTab()
		{
            InitializeComponent();
            StockVm = StockViewModel.instance;
            BindingContext = StockVm;
            searchStock.Text = "AMD";
            GetStocks(null, null);
        }

        public async void GetStocks(object sender, EventArgs e)
        {
            var client = new HttpClient();
            const string apiKey = "EJCM9R0V9YN5GONF";
            var getStockByName = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=";
            Uri uri;

            getStockByName += searchStock.Text + "&apikey=" + apiKey;
            uri = new Uri(getStockByName);
            try
            {
                StockResponse stockResp = new StockResponse();
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    stockResp = JsonConvert.DeserializeObject<StockResponse>(jsonContent);
                    StockVm.GetStockByName(stockResp, searchStock.Text);
                }
            }
            catch (Exception err)
            {
                if (err.GetType() == typeof(NullReferenceException))
                {
                    await DisplayAlert("Error", "Invalid stock symbol", "OK");
                }
                else
                {
                    await DisplayAlert("Error", "Request error. Check your internet connection and try again. " + err.ToString(), "OK");
                }
            }
        }
    }
}