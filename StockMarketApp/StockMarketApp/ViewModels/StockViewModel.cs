﻿using Microcharts;
using Newtonsoft.Json;
using SkiaSharp;
using StockMarketApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;
using Entry = Microcharts.Entry;

namespace StockMarketApp.ViewModels
{
    class StockViewModel: INotifyPropertyChanged
    {
        private ObservableCollection<Stock> _stocks;
        private string _name;
        private string _high;
        private string _low;
        private static StockViewModel _instance;

        public ObservableCollection<Stock> stocks
        {
            get { return _stocks; }
            set
            {
                _stocks = value;
                OnPropertyChanged();
            }
        }

        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string high
        {
            get { return _high; }
            set
            {
                _high = value;
                OnPropertyChanged();
            }
        }

        public string low
        {
            get { return _low; }
            set
            {
                _low = value;
                OnPropertyChanged();
            }
        }

        public static StockViewModel instance
        {
            get => _instance ?? (_instance = new StockViewModel());
        }

        public StockViewModel()
        {
            name = "AMD";
        }

        public void SetHighLowValue()
        {
            string tmpHigh = "";
            string tmpLow = "";
            tmpHigh = stocks[0].high;
            tmpLow = stocks[0].low;

            foreach (Stock item in stocks)
            {
                if (Convert.ToDouble(tmpHigh) < Convert.ToDouble(item.high))
                {
                    tmpHigh = item.high;
                }

                if (Convert.ToDouble(tmpLow) > Convert.ToDouble(item.low))
                {
                    tmpLow = item.low;
                }
            }
            high = "Highest $" + tmpHigh;
            low = "Lowest $" + tmpLow;
        }

        public void GetStockByName(StockResponse stockResp, string search)
        {
            if (!Equals(stockResp.data, null))
            {
                name = search;
            }
            foreach (KeyValuePair<string, Stock> stock in stockResp.data)
            {
                stock.Value.day = DateTime.ParseExact(stock.Key, "yyyy-MM-dd",
                                       System.Globalization.CultureInfo.InvariantCulture).ToString("MMMM dd");
            }
            stocks = new ObservableCollection<Stock>(stockResp.data.Values);
            SetHighLowValue();
        }
       

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
