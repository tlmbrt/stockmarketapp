﻿using Newtonsoft.Json;
using StockMarketApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace StockMarketApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchBarPage : ContentPage
    {
        private string apiKey;
        private string searchedText;

        public SearchBarPage(string text)
        {
            InitializeComponent();
            apiKey = "EJCM9R0V9YN5GONF";
            search.Text = string.IsNullOrEmpty(text) ?
                "" :
                text;
        }

        async void GetAllStockName(object sender, EventArgs e)
        {
            var client = new HttpClient();
            var stockNameApi = "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=";
            Uri uri;

            searchedText = search.Text;
            Console.WriteLine(searchedText);
            stockNameApi += searchedText + "&apikey=" + apiKey;
            uri = new Uri(stockNameApi);

            StockNameResponse symbols = new StockNameResponse();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                symbols = JsonConvert.DeserializeObject<StockNameResponse>(jsonContent);
            }
            symbolsListView.ItemsSource = new ObservableCollection<StockName>(symbols.data);
        }

        async void GoBackSearchResult(object sender, System.EventArgs e)
        {
            //await Navigation.PushAsync(new SearchBarPage(searchStock.Text));
        }
    }
}